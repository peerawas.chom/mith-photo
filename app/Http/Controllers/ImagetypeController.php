<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\imagetype;
class ImagetypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $imagetype = imagetype::orderBy('id','asc')->get();
        return view('gallery.imagetype',[
            'imagetype'=> $imagetype
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('gallery.addtype');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $store = new imagetype();
            $store->type = $request->type;
            $store->save();

            return redirect('')->route('');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $imagetype = imagetype::where('id',$id)->get();
        return view('gallery.edittype',[
            'imagetype'=> $imagetype
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = imagetype::find($id);

        $update->type = $request->typename;
        $update->save();

        return redirect('/type');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function del($id)
    {
        imagetype::find($id)->delete();
        return response([]);

    }
}
