<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class image extends Model
{
    protected $table = "gallery";
    public $timestamps = false;
    //created_at timestramp
    //updated_at timestramp
}
