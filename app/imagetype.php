<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class imagetype extends Model
{
    protected $table = "imagetype";
    public $timestamps = false;
}
