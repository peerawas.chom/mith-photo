@extends('layout.layout')
@section('contents')

<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">เพิ่มประเภทรูปภาพ</h6>
      <a href="{{url('/type')}}" class="btn btn-danger btn-icon-split" >
        <span class="text">ย้อนกลับ</span>
      </a>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <form method="POST" action="{{route('type.store')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="exampleInputEmail1">ประเภทรูปภาพ</label>
              <input type="text" class="form-control" id="type" name="type" placeholder="ชื่อประเภท">
            </div>
            <button type="submit" class="btn btn-success">เพิ่ม</button>
          </form>
    </div>
  </div>
</div>
@endsection
