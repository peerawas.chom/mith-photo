@extends('layout.layout')
@section('contents')

<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">เพิ่มรูปภาพ</h6>
      <a href="{{url('/')}}" class="btn btn-danger btn-icon-split" >
        <span class="text">ย้อนกลับ</span>
      </a>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <form method="POST" action="{{route('galleries.store')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="exampleInputEmail1">Images</label>
              <input type="file" class="form-control" id="image" name="image" placeholder="File Image">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Caption</label>
              <input type="text" class="form-control" id="caption" name="caption" placeholder="Caption">
            </div>
            <button type="submit" class="btn btn-success">เพิ่ม</button>
          </form>
    </div>
  </div>
</div>
@endsection
