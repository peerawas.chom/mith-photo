@extends('layout.layout')
@section('contents')

<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">แก้ไขรูปภาพ</h6>
      <a href="{{url('/')}}" class="btn btn-danger btn-icon-split" >
        <span class="text">ย้อนกลับ</span>
      </a>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <form method="post" action="{{route('galleries.update',['gallery'=> $image[0]->id])}}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
              <label for="exampleInputEmail1">Images</label>
              <img src="{{env('APP_PUBLIC')}}storage/image/{{$image[0]->image}}" width="200px">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Caption</label>
              <input type="text" class="form-control" id="caption" name="caption" placeholder="Caption" value="{{$image[0]->caption}}">
            </div>
            <button type="submit" class="btn btn-success">บันทึก</button>
          </form>
    </div>
  </div>
</div>
@endsection
