@extends('layout.layout')
@section('contents')

<div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">แก้ไขประเภทรูปภาพ</h6>
      <a href="{{url('/type')}}" class="btn btn-danger btn-icon-split" >
        <span class="text">ย้อนกลับ</span>
      </a>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <form method="post" action="{{route('type.update',['type'=> $imagetype[0]->id])}}" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
              <label for="exampleInputPassword1">Caption</label>
              <input type="text" class="form-control" id="typename" name="typename"  value="{{$imagetype[0]->type}}">
            </div>
            <button type="submit" class="btn btn-success">บันทึก</button>
          </form>
    </div>
  </div>
</div>
@endsection
