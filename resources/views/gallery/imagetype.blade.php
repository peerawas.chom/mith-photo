@extends('layout.layout')
@section('contents')


<h1 class="h3 mb-2 text-gray-800">ประเภทรูปภาพ</h1>
          <p class="mb-4">ประเภทรูปภาพทั้งหมด
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">รายการประเภทรูปภาพ</h6>
              <a href="{{route('type.create')}}" class="btn btn-primary btn-icon-split" >
                <span class="text">เพิ่มประเภทรูปภาพ</span>
              </a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>ลำดับ</th>
                      <th>ประเภท</th>
                      <th>Edit</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $count=0;
                    ?>
                    @foreach ($imagetype as $imagetypes)
                    <tr>
                      <td>{{$count+=1}}</td>
                      <td>{{$imagetypes->type}}</td>
                      <td> <a href="{{url('type/'.$imagetypes->id.'/edit')}}"  class="btn btn-warning btn-icon-split">
                        <span class="text">แก้ไข</span>
                      </a>
                      <button class="btn btn-danger btn-icon-split bt-del" data-id="{{$imagetypes->id}}">
                        <span class="text">ลบ</span>
                      </button></td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script>

            let bt_del = $('.bt-del')
            bt_del.click(function(){
                $.ajax({
                    type:'get',
                    url: "{{ url('/deletetype/') }}"+"/"+$(this).attr("data-id"),
                    data:'_token = <?php echo csrf_token() ?>',
                    success:function(data) {
                        location.reload();
                    }
                });
            })

        </script>

@endsection
