<?php

use App\Http\Controllers\GalleryController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('galleries.index');
});

Route::resource('/galleries','GalleryController');
Route::get('/deleteimage/{id}','GalleryController@del')->name('deleteimage');

Route::resource('/type','ImagetypeController');
Route::get('/deletetype/{id}','ImagetypeController@del')->name('deletetype');
